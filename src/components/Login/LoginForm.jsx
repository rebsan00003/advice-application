import '../../App.css';
import keycloak from '../../keycloak/keycloak';
import { useNavigate } from "react-router-dom";

const LoginForm = () => {
    let navigate = useNavigate();

    const handleLogIn = () => {
        //redirect to keycloak to login
        navigate("/advice")
        keycloak.login() 
    }
    
    return (
        <>
        <div className="LoginCard" >
                <p className='Welcometxt'>Welcome to Advice App</p>
                <button className="button-58" role="button" onClick={handleLogIn}>Sign In With Key Cloak</button>
        </div>
        </>
    )
}
export default LoginForm