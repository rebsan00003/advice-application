
import '../../App.css';
import keycloak from '../../keycloak/keycloak';

const handleLogOut = () => {
    keycloak.logout();
}


//the layout of the header that follows with every page
const Header = () => {
    return (
        <div className='Header'>
            <h2 className='Title'>Advice app</h2>
            { keycloak.authenticated ? <a className='LogoutBtn' onClick={handleLogOut}>Log out</a> : <p></p>}
            
        </div>
    ) 
}

export default Header