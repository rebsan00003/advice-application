import './App.css';
import Header from './components/Header/Header';
import Login from './views/Login';
import {
  BrowserRouter,
  Routes,
  Route
} from 'react-router-dom';
import Advice from './views/Advice';
import Profile from './views/Profile';
import LoggedInRoute from './routes/LoggedInRoute';


function App() {
  return (
    <BrowserRouter>
      <div className="App">
      <Header></Header>
      <Routes>
        <Route path="/" element={<Login/>}/>
        <Route path="/advice" element={
          <LoggedInRoute>
            <Advice/>
          </LoggedInRoute>
        }/>
        <Route path="/profile" element={
          <LoggedInRoute>
            <Profile></Profile>
          </LoggedInRoute>
        }/>
      </Routes>
    </div>
    </BrowserRouter>
  );
}

export default App;