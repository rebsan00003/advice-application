import LoginForm from "../components/Login/LoginForm";


//the layout of the Login page 
import '../App.css';

const Login = () => {
  return (
    <>
      <div className="BackgroundBox">
        <div className="svgContainer">
            <img className="LoginPageImg" src="undraw_blooming_re_2kc4.svg"></img>
        </div>
        <LoginForm></LoginForm>
      </div>
      
    </>
  );
};

export default Login;
