import { useEffect, useState } from 'react';

import '../App.css';

//the layout of the Translation page 
const Advice = () => {

    const [ advice, setAdvice ] = useState("");
    const [ fadeIn, setFadeIn ] = useState(false);

    const fetchAdvice = async () => {
        const response = await fetch("https://api.adviceslip.com/advice");
        const data = await response.json()
        setAdvice(JSON.stringify(data.slip.advice))

        animate();
    }

    useEffect(() => {
        fetchAdvice()
    }, [])

    const animate = () => {
        setFadeIn(true);
        setTimeout(() => setFadeIn(false), 2000)
    }

    return (
        <>
        <div className="AdviceContainer">
            <h2 className='AdviceTitle'>Your Advice</h2>
            <h3 className={fadeIn ? 'Advicetxt' : 'AdvicetxtDefault' }>{advice}</h3>
            <div><button className='button-58 AdviceBtn' onClick={fetchAdvice}>New Advice</button></div>
            
        </div>
        
        </>
    );
  };
  
  export default Advice