import keycloak from "../keycloak/keycloak";
import {Navigate} from 'react-router-dom';


function LoggedInRoute( { children }){
    if(keycloak.authenticated){
        return(
            <>
            {children}
            </>
        )
    }else {
        return <Navigate to="/"></Navigate>
    }
}

export default LoggedInRoute